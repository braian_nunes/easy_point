#!/bin/bash

# Ignorar os erros de instalação
#set -e

DIR_SCRIPT=~/Documents/bitbar/scripts
DIR_SCRIPT_CONF=~/Documents/bitbar/config
PYTHON_SCRIPT='easypoint.1m.py'
CONF_FILE='easypoint.ini'

echo "      ___           ___           ___           ___           ___           ___                       ___           ___      "
echo "     /\  \         /\  \         /\  \         |\__\         /\  \         /\  \          ___        /\__\         /\  \     "
echo "    /::\  \       /::\  \       /::\  \        |:|  |       /::\  \       /::\  \        /\  \      /::|  |        \:\  \    "
echo "   /:/\:\  \     /:/\:\  \     /:/\ \  \       |:|  |      /:/\:\  \     /:/\:\  \       \:\  \    /:|:|  |         \:\  \   "
echo "  /::\~\:\  \   /::\~\:\  \   _\:\~\ \  \      |:|__|__   /::\~\:\  \   /:/  \:\  \      /::\__\  /:/|:|  |__       /::\  \  "
echo " /:/\:\ \:\__\ /:/\:\ \:\__\ /\ \:\ \ \__\     /::::\__\ /:/\:\ \:\__\ /:/__/ \:\__\  __/:/\/__/ /:/ |:| /\__\     /:/\:\__\ "
echo " \:\~\:\ \/__/ \/__\:\/:/  / \:\ \:\ \/__/    /:/~~/~    \/__\:\/:/  / \:\  \ /:/  / /\/:/  /    \/__|:|/:/  /    /:/  \/__/ "
echo "  \:\ \:\__\        \::/  /   \:\ \:\__\     /:/  /           \::/  /   \:\  /:/  /  \::/__/         |:/:/  /    /:/  /      "
echo "   \:\ \/__/        /:/  /     \:\/:/  /     \/__/             \/__/     \:\/:/  /    \:\__\         |::/  /     \/__/       "
echo "    \:\__\         /:/  /       \::/  /                                   \::/  /      \/__/         /:/  /                  "
echo "     \/__/         \/__/         \/__/                                     \/__/                     \/__/                   "
echo ""
echo ""
echo ""

echo "Instalando Homebrew"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

echo "Instalando Python 3..."
brew install python3

echo "Instalando dependências..."
pip3 install -r requirements.txt

echo "Instalando bitbar..."
brew cask install bitbar

echo "Configurando diretório do plugin..."
mkdir -p  $DIR_SCRIPT

echo "Configurando diretório do arquivo de configuração..."
mkdir -p $DIR_SCRIPT_CONF

echo "Movendo script..."
cp $PYTHON_SCRIPT $DIR_SCRIPT

echo "Movendo arquivos de configuração para a pasta de destino..."
cp $CONF_FILE $DIR_SCRIPT_CONF

echo "Conceder permissão nos diretórios e arquivos..."
chmod -R +x $DIR_SCRIPT
chmod -R +x $DIR_SCRIPT_CONF

# "Alterando parametros no script"
read -p "Informe o seu usuário: "  username

# Replace do usuário
sed -i -e 's/user =/user = '$username'/g' "$DIR_SCRIPT_CONF/$CONF_FILE"

echo "Informe a sua senha de rede: "
read -s password

# Replace da senha
sed -i -e 's/pass =/pass = '$password'/g' "$DIR_SCRIPT_CONF/$CONF_FILE"

# Testando script
#echo "Executando script para teste"
#python3 "$DIR_SCRIPT/$PYTHON_SCRIPT"

# Executando o Bitbar
open -a BitBar

# Aviso para alteração do diretório do plugin
echo ""
echo ""
echo "############# AVISO #############"
echo "~> É necessário configurar a pasta de plugins do BitBar para o diretório: $DIR_SCRIPT"
echo "Instruções:"
echo "1 - Clique no ícone do BitBar"
echo "2 - Preferences/Change Plugin Folder..."
echo "3 - Selecione o diretório: $DIR_SCRIPT"