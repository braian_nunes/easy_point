EasyPoint
=====

EasyPoint tem como finalidade controlar o ponto eletrônico do funcionário. 

Utiliza o [BitaBar](https://getbitbar.com) para exibir as informações do ponto eletrônico através de uma barra de menu.



## Instruções

1. Clonar o projeto através do repositório 

   ```
   git clone https://braian_nunes@bitbucket.org/braian_nunes/easy_point.git
   ```

2. Executar o script de instalação __easypoint_setup.sh__ através do terminal

   ```
   ./easypoint_setup.sh
   ```

3. Configurar o diretório de plugins no BitBar

   1. Abrir o __BitBar__

   2. _Preferences/Change Plugin Folder…_

   3. Apontar para o diretório oficial de Plugins: __~/Documents/bitbar/scripts__

      

![alt text](https://i.ibb.co/hsMGDPc/plugin-folder.png "Plugin folder")
      

   