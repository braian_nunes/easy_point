#!/usr/local/Cellar/python/3.7.3/Frameworks/Python.framework/Versions/3.7/bin/python3
# TODO Configurar variável de ambiente para utilizar /python3

# -*- coding: utf-8 -*-
# <bitbar.title>Easypoint</bitbar.title>
# <bitbar.version>v0.1</bitbar.version>
# <bitbar.author>Braian Nunes</bitbar.author>
# <bitbar.author.github>/BraianNunes</bitbar.author.github>
# <bitbar.desc>Controle de ponto eletrônico do funcionário</bitbar.desc>
# <bitbar.dependencies>python3</bitbar.dependencies>
#

import json
import requests
import configparser
import os
from datetime import datetime, timedelta

HEADERS = {
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.9,pt;q=0.8',
    'Content-Type': 'application/json',
    'Host': 'm.sippe.com.br',
}

# URLs
login_url = '/forponto-rest/login'
employee_details_url = '/forponto-rest/resumoFuncionario'
electronic_point_registers_url = '/forponto-rest/registros'
base_url = 'https://m.sippe.com.br'

# Dicionário de display
display = {
    'ponto': '',
    'saida_sugerida': '',
    'duracao_almoco': '',
    'tempo_restante': '',
}

# Variáveis do arquivo de configuração
config_parameter = {}

# Path do arquivo de configuração
DIR_SCRIPT_CONF= "~/Documents/bitbar/config"
SCRIPT_CONF_FILE="easypoint.ini"
config_file_path = None

# Cores
GREEN = '#29cc00'
RED = '#ff0000'
YELLOW = '#ffff00'

# Constantes
HOUR_MINUTE_FORMAT = "%H:%M"
HOUR_MINUTE_SECOND_FORMAT = "%H:%M:%S"
DATE_FORMAT = "%d-%m-%y"
DATE_TIME_FORMAT = "%d-%m-%y %H:%M:%S"
TOTAL_HORA_TRABALHO = 8
TOTAL_HORA_ALMOCO = 1

# Variáveis globais
horas_restantes = '08:00'
chegada = None
saida = None
chegada_almoco = None
duracao_almoco = None
duracao_almoco_display = None
saida_almoco = None
hora_sugerida_saida = None
pontos = []


def get_config_parameter(param, selection='default'):
    return config_parameter[selection][param]


def get_authorization_token():
    try:
        # Se o token já existir no arquivo de configuração eu utilizo ele
        # token = get_config_parameter('token')
        # if token:
        #     return token

        # Payload para efetuar login
        login_credential = {
            "usuario": config_parameter['default']['user'],
            "senha": config_parameter['default']['pass'],
        }

        with requests.Session() as s:
            r = s.post(f'{base_url}{login_url}', headers=HEADERS, data=json.dumps(login_credential),
                       allow_redirects=True)
            if r.status_code != 200:
                # "Usuário/senha inválido(s)!
                return None
            else:
                return r.headers['Authorization'] 
    except Exception as e:
        raise ValueError(f'Usuário/senha inválido(s)')


def get_token():
    try:
        token = get_config_parameter('token')
        refresh_token = int(get_config_parameter('refresh_token_interval'))
        latest_update = get_config_parameter('latest_update')

        if not token:
            token = get_authorization_token()
            if not token:
                raise ValueError('Usuário/senha inválido(s)')

            update_property('token', token)

        if latest_update:
            latest_update = str_to_datetime(latest_update, DATE_TIME_FORMAT)

            # Se a data do último login for maior que 5 dias ou
            # o token está em branco, fazemos um novo login.
            diff_date_login = datetime.now() - latest_update
            if diff_date_login.days > refresh_token:
                token = get_authorization_token()
                update_property('token', token)

        return token
    except Exception as e:
        raise ValueError(f'Erro ao obter token. Motivo: {e}')


def check_marks(authorization_token):
    currentDate = datetime.now()
    initialDate = currentDate.strftime("%Y-%m-%d")
    initialDatePlus1 = (currentDate + timedelta(days=1)).strftime("%Y-%m-%d")

    HEADERS['Authorization'] = authorization_token

    PAYLOAD = {
        'dataInicio': initialDate,
        'dataFim': initialDatePlus1,
    }

    with requests.Session() as s:
        r = s.post(f'{base_url}{electronic_point_registers_url}', headers=HEADERS, data=json.dumps(PAYLOAD),
                   allow_redirects=True)
        if r.status_code == 500:
            # Se o token foi extraído do arquivo de configuração ele pode estar expirado/inválido
            if get_config_parameter('token'):
                # print('Token expirado. Atualizando....')
                config_parameter['default']['token'] = None
                new_token = get_token()
                return check_marks(new_token)
            else:
                # Se o token estiver vencido, realiza um novo login
                print("Falha de autenticação.")

        json_parsed = json.loads(r.text)
        registros_list = json_parsed['registros']
        for i in registros_list:
            date = i['dia']
            # ts = time.gmtime()
            i['dia'] = datetime.fromtimestamp(date / 1000.0).strftime("%Y-%m-%d")
            if (i['dia'] == initialDate):
                pontos_list = i['pontos']
                for j in pontos_list:
                    pontos.append(j)
                break

    # Efetua a ligação das marcações com as variáveis (chegada, saida_almoco, chegada_almoco e saida)
    assign_marks(pontos)

    # Atualiza a variável 'ponto' do display
    display['ponto'] = pontos

    # Efetua o cálculo dos horários
    calc_electronic_point()

    # Exibe o display (print) utilizado no Bitbar
    show_display()


def str_to_datetime(strTime, format=HOUR_MINUTE_FORMAT):
    return datetime.strptime(strTime, format)


def time_to_hour(timedelta):
    return timedelta.seconds // 3600


def time_to_minute(timedelta):
    return int(timedelta.seconds / 60)


def calc_tempo_restante():
    hora_atual = datetime.now()
    return timedelta(hours=9) - (hora_atual - chegada)


def calc_electronic_point():
    """ Exemplo:
       chegada = 10:00
       saida_almoco = 12:00
       chegada_almoco = 12:30
       hora_saida_sugerida = 19:00
       hora_saida = **

       00 - Para calcular a hora restante: Calcula-se a diferença do horário de chegada - a hora total de trabalho
           chegada=10:00
           hora_atual=11:00
           hora_restante = 11:00 - 10:00 = 01:00
           hora_restante = hora_total - 01:00
           hora_restante = -07:00

       01 - Se tem apenas o horário de entrada, calcula-se: chegada + hora_total + 1
           10:00 + 8h + 1h (almoço) = 19:00

       02 - Se tem saida_almoco: (saida_almoco - chegada) - hora_total + 1
           12:00 - 10:00 = 2h - (8h + 1h) => -7h (restantes de trabalho)

       03 - Se tem chegada_almoco: (chegada_almoco - saida_almoco)
           13:00 - 12:00 = 1h => hora_saida_sugerida + 1h => 19:00

       04 - Se tem chegada do almoço: Cálculo para saber a quantidade de horas restantes
           13:00 - 12:00 = 1h => horas_restantes + horas_de_almoco """

    global chegada
    global saida
    global chegada_almoco
    global duracao_almoco
    global saida_almoco
    global hora_sugerida_saida
    global duracao_almoco
    global duracao_almoco_display

    # Caso 00 - Cálculo da hora_restante com base no horário de chegada
    if chegada:
        horas_restantes = calc_tempo_restante()
        # print(f'Horas restantes: {horas_restantes}')
        # Caso 01
        hora_sugerida_saida = chegada + timedelta(hours=TOTAL_HORA_TRABALHO + TOTAL_HORA_ALMOCO)
        # print(f'Hora de saída sugerida: {hora_sugerida_saida}')
    else:
        print('Você não bateu o ponto hoje!')

    # Caso 02
    if saida_almoco:
        # inteiro representando as horas restantes
        horas_restantes = (saida_almoco - chegada)
        # print(f'Quanto tempo desde a chegada até a saída do almoço: {horas_restantes}')
        horas_restantes = calc_tempo_restante()
        # print(f'Horas restantes: {horas_restantes}')

    # TODO Aviso, almoço com duração superior a 60 minutos

    # Caso 03
    if chegada_almoco:
        duracao_almoco = (chegada_almoco - saida_almoco)
        duracao_almoco_display = duracao_almoco
        # print(f'Duração almoço: {duracao_almoco}')
        duracao_almoco_minutos = time_to_minute(duracao_almoco)
        # print(f'Duração do almoço em minutos: {duracao_almoco_minutos}')

        if (duracao_almoco_minutos > 60):
            # Se a duração do almoço for maior que uma hora, soma-se o resultado com o tempo restante a partir da primeira hora
            diff_duracao_almoco = duracao_almoco - timedelta(hours=TOTAL_HORA_ALMOCO)
            # print(f'Diff duração almoço: {diff_duracao_almoco}')
            horas_restantes = horas_restantes + diff_duracao_almoco
            hora_sugerida_saida = hora_sugerida_saida + diff_duracao_almoco
            # print(f'Horas Restantes: {horas_restantes}')

        else:
            # almoco menor que 1 hora
            diff_duracao_almoco = timedelta(hours=TOTAL_HORA_ALMOCO) - duracao_almoco
            # print(f'Diff duracao almoco p/ horas restantes: {diff_duracao_almoco}')
            horas_restantes = horas_restantes - diff_duracao_almoco

    # Se não houver saída adiciono o tempo restante na última posição do dicionário de pontos
    if chegada and saida is None:
        total_hora_trabalho_restante = time_to_hour(horas_restantes)
        # print(f'Tempo restante em horas: {total_hora_trabalho_restante}')
        if total_hora_trabalho_restante > 9:
            horas_restantes = timedelta(days=1) - horas_restantes
            # print(f'Horas restantes: {horas_restantes}')
            str_time_delta = f'+{time_delta_to_string(horas_restantes.total_seconds())} | color={GREEN}'
        else:
            str_time_delta = f'-{time_delta_to_string(horas_restantes.total_seconds())} | color={RED}'
        # print(f'strTimedelta: {str_time_delta}')
        pontos.append(str_time_delta)


def time_delta_to_string(seconds):
    seconds = int(seconds)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if hours > 0:
        return '%s:%s:%s' % (str(hours).zfill(2), str(minutes).zfill(2), str(seconds).zfill(2))
    elif minutes > 0:
        return '%s:%s:%s' % ('00', str(minutes).zfill(2), str(seconds).zfill(2))
    else:
        return '%s:%s:%s' % ('00', '00', str(seconds).zfill(2),)


def assign_marks(pontos):
    global chegada
    global saida_almoco
    global chegada_almoco
    global saida
    global duracao_almoco

    if len(pontos) > 0 and pontos[0]:
        chegada = datetime.strptime(pontos[0], HOUR_MINUTE_FORMAT)
    if len(pontos) > 1 and pontos[1]:
        saida_almoco = datetime.strptime(pontos[1], HOUR_MINUTE_FORMAT)
    if len(pontos) > 2 and pontos[2]:
        chegada_almoco = datetime.strptime(pontos[2], HOUR_MINUTE_FORMAT)
    if len(pontos) > 3 and pontos[3]:
        saida = datetime.strptime(pontos[3], HOUR_MINUTE_FORMAT)


def show_display():
    # Exibição das marcações no Bitbar
    if display['ponto']:
        dPonto = ''
        for i in display['ponto']:
            dPonto = dPonto + i + ' '
        print(f'{dPonto}')

    # Exibição da hora sugerida de saída
    if hora_sugerida_saida:
        print('---')
        print(f'Saída sugerida: {date_time_to_string(hora_sugerida_saida)} | color={YELLOW}')

    # Duração do almoço
    if duracao_almoco:
        print('---')
        print(f'Duração do almoço: {duracao_almoco_display}')


def date_time_to_string(time):
    return None if time is None else time.strftime(HOUR_MINUTE_FORMAT)


def update_property(property, value, section='default'):
    config = configparser.ConfigParser()
    config.read(config_file_path)
    config.set(section, property, value)

    # Para todas as alterações realizadas no arquivo, a data da última atualização ocorrerá automaticamente
    config.set(section, 'latest_update', datetime.now().strftime(DATE_TIME_FORMAT))
    with open(config_file_path, 'w') as configfile:
        config.write(configfile)


def simulate_checkpoint():
    marks = [
        '10:00',
        '12:00',
        '13:00',
    ]
    display['ponto'] = marks
    assign_marks(marks)
    calc_electronic_point()
    show_display()


def read_config_file():
    global config_parameter
    global config_file_path
    try:
        config_file_path = os.path.expanduser(f'{DIR_SCRIPT_CONF}/{SCRIPT_CONF_FILE}')
        config = configparser.ConfigParser()
        config.read(config_file_path)
        config_parameter = {s: dict(config.items(s)) for s in config.sections()}
        # print(config_parameter)
        if not config_parameter or (
                (not get_config_parameter('user')
                 or not get_config_parameter('pass')) and not get_config_parameter('token')):
            raise ValueError(f'Arquivo inconsistente {config_file_path}')
    except Exception as e:
        raise ValueError(f'Não foi possível ler o arquivo de configurações. Motivo: {e}')


if __name__ == '__main__':
    try:
        read_config_file()
        token = get_token()
        check_marks(token)
    except Exception as e:
        print(f'{e}')

